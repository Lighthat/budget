﻿using System;
using System.Collections.Generic;
namespace budget
{
	public class Model : IObservable<string>
	{
		private IObserver<string> _observer;
		private string _dataFilePath;
		private string _configFilePath;
		List<BudgetItem> _itemList = new List<BudgetItem>();

		/// <summary>
		/// Initializes a new instance of the <see cref="budget.Model"/> class.
		/// It will automatically set the default path of the data file in the current working directory
		/// ...At least that's the desired behavior, the current implementation is a temporary workaround
		/// so I don't have to mess with premissions on my local machine.
		/// 
		/// It will create the data file if it doesn't already exist, and it will also set up a config file.
		/// The config file just lets the program know where to look for the data file in case the user ever
		/// changes the path of their data file.
		/// </summary>
		public Model ()
		{
			//These should be updated before the final release to create things in the working directory.
			_dataFilePath = "/home/caghyarian/Documents/BudgetDir/bin/data.df";
			System.IO.Directory.CreateDirectory ("/home/caghyarian/Documents/BudgetDir/bin");
			_configFilePath = "/home/caghyarian/Documents/BudgetDir/bin/config.cf";
			if(!System.IO.File.Exists(_configFilePath))
			{
				var file = System.IO.File.Create (_configFilePath);
				file.Close ();
				System.IO.File.WriteAllText (@_configFilePath, _dataFilePath);
			}
			if(!System.IO.File.Exists(_dataFilePath))
			{
				var file = System.IO.File.Create (_dataFilePath);
				file.Close ();
			}
		}

		/// <summary>
		/// Loads data from the data file.
		/// </summary>
		/// <param name="controller">The active Controller. This is needed because creating BudgetItems requires access to the Controller so it can add its click events.</param>
		/// <param name="view">The active View where the BudgetItems should be displayed.</param>
		public void Load(Controller controller, View view)
		{
			_itemList = BudgetUtility.Load (_dataFilePath, controller);
			foreach (BudgetItem item in _itemList)
				view.AddBudgetItem (item);
		}

		/// <summary>
		/// Adds the given BudgeItem to the View.
		/// </summary>
		/// <param name="item">Item.</param>
		public void AddBudgetItem (BudgetItem item)
		{
			_itemList.Add (item);
			BudgetUtility.Save (_itemList, _dataFilePath);
			_observer.OnNext ("add_item: " + item.ToString() );
		}

		public void RemoveBudgetItem (BudgetItem item)
		{
			BudgetItem itemToRemove = null;
			foreach (BudgetItem __item in _itemList) 
			{
				if (__item.ItemName () == item.ItemName ())
					itemToRemove = __item;
			}
			_itemList.Remove (itemToRemove);
			BudgetUtility.Save (_itemList, _dataFilePath);
			_observer.OnNext ("remove_item: " + item.ToString ());
		}

		/// <summary>
		/// Returns the list of items in this instance.
		/// </summary>
		/// <returns>The list.</returns>
		public List<BudgetItem> ItemList () {return _itemList;}

		/// <summary>
		/// Sets the path of the data file.
		/// </summary>
		/// <param name="path">The file path.</param>
		public void DataFilePath(string path)
		{
			_dataFilePath = path;
			System.IO.File.WriteAllText (_configFilePath, path);
		}

		/// <summary>
		/// Returns the path to the data file where the budget information in saved
		/// </summary>
		/// <returns>The file path.</returns>
		public string DataFilePath()
		{
			return _dataFilePath;
		}

		public void AllocatePaycheck(double amount)
		{
			double runningSum = 0.00;
			double __amount = amount;
			foreach (BudgetItem item in ItemList()) 
			{
				runningSum += item.AmountPerCheck();
			}
			if (runningSum >= amount) {
				DialogUtility.RunErrorDialog ("Could not allocate the funds. You tried to allocate " + amount.ToString("C")  + " but your budget calls for a minimum of " + runningSum.ToString("C"));
				return;
			}

			//Add money to the items without a percentage first.
			foreach (BudgetItem item in ItemList()) {
				item.AddFlatMoneyFromCheck ();
				__amount -= item.AmountPerCheck();
			}
			//At this point, __amount contains the remaining money.
			//We can now start allocating money to the budget items that require a percentage.
			foreach(BudgetItem item in ItemList()) {
				item.AddPercentMoneyFromCheck (__amount);
				Debug.Out (__amount + "");
			}
			BudgetUtility.Save(ItemList(), DataFilePath());


		}

		/// <returns>An Unsubscriber that can allow the View to clean up when the program terminates.</returns>
		/// <summary>
		/// Connects the Model to the View
		/// </summary>
		/// <param name="observer">The View</param>
		public IDisposable Subscribe(IObserver<string> observer) 
		{
			_observer = observer;
			return new Unsubscriber(observer);
		}

		/// <summary>
		/// The Unsubscriber class that allows observers to clean up an observable object
		/// </summary>
		private class Unsubscriber : IDisposable
		{
			private IObserver<string> __observer;

			/// <summary>
			/// Initializes a new instance of the <see cref="networked.Model+Unsubscriber"/> class.
			/// </summary>
			/// <param name="observer">The View</param>
			public Unsubscriber(IObserver<string> observer)
			{
				__observer = observer;
			}

			/// <summary>
			/// Releases all resource used by the <see cref="networked.Model+Unsubscriber"/> object.
			/// </summary>
			/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="networked.Model+Unsubscriber"/>. The
			/// <see cref="Dispose"/> method leaves the <see cref="networked.Model+Unsubscriber"/> in an unusable state. After
			/// calling <see cref="Dispose"/>, you must release all references to the <see cref="networked.Model+Unsubscriber"/>
			/// so the garbage collector can reclaim the memory that the <see cref="networked.Model+Unsubscriber"/> was occupying.</remarks>
			public void Dispose()
			{
				Debug.Out (__observer.GetHashCode () + " Unsubscribe.");
			}
		}
	}
}

