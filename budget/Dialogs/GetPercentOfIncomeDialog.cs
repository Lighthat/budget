﻿using System;
using Gtk;

namespace budget
{
	public partial class GetPercentOfIncomeDialog : Dialog
	{

		Entry _entry;
		Label _label;
		Button _button;
		Controller _controller;

		int _percentOfIncome;

		public GetPercentOfIncomeDialog (Controller controller) : base("Add Budget Item", null, DialogFlags.Modal)
		{
			_controller = controller;
			_label  = new Label  ("Enter the percent of your income you want allocated per check.");
			_entry  = new Entry  ("0");
			_button = new Button ("Enter");

			_entry.IsEditable = true;
			_entry.Visibility = true;
			_button.Clicked += new EventHandler (OnPercentOfIncomeEntryEvent);

			this.VBox.Add (_label);
			this.VBox.Add (_entry);
			this.VBox.Add (_button);

			this.Resize (50, 150);
			this.ShowAll ();
		}

		public int Value()
		{
			return _percentOfIncome;
		}

		protected void OnPercentOfIncomeEntryEvent(object sender, EventArgs args)
		{
			Debug.Out("Percent of income entered.");
			try{
				_percentOfIncome = Int32.Parse(_entry.Text);
				this.Destroy();
			}
			catch(Exception e)
			{
				DialogUtility.RunErrorDialog("Please enter a a valid number.");
				Debug.Out (e.Message);
				return;
			}
		}
	}
}

