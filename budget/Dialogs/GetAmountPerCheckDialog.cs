﻿using System;
using Gtk;

namespace budget
{
	public partial class GetAmountPerCheckDialog : Dialog
	{

		Entry _entry;
		Label _label;
		Button _button;
		Controller _controller;

		double _amountPerCheck;

		public GetAmountPerCheckDialog (Controller controller) : base("Add Budget Item", null, DialogFlags.Modal)
		{
			_controller = controller;
			_label  = new Label  ("Enter the amount of money allocated per check.");
			_entry  = new Entry  ("0.00");
			_button = new Button ("Enter");

			_entry.IsEditable = true;
			_entry.Visibility = true;
			_button.Clicked += new EventHandler (OnAmountPerCheckEntryEvent);

			this.VBox.Add (_label);
			this.VBox.Add (_entry);
			this.VBox.Add (_button);

			this.Resize (50, 150);
			this.ShowAll ();
		}

		public double Value()
		{
			return _amountPerCheck;
		}

		protected void OnAmountPerCheckEntryEvent(object sender, EventArgs args)
		{
			Debug.Out("Amount per check entered.");
			try{
				_amountPerCheck = Double.Parse(_entry.Text);
				this.Destroy();
			}
			catch(Exception e)
			{
				DialogUtility.RunErrorDialog("Please enter a a valid number.");
				Debug.Out (e.Message);
				return;
			}
		}
	}
}

