﻿using System;
using Gtk;

namespace budget
{
	/// <summary>
	/// Provides a dialog for the user to enter the amount they got paid so it can be automatically allocated.
	/// </summary>
	public partial class PaycheckInputDialog : Dialog
	{
		Entry _entry;
		Label _label;
		Button _button;
		Controller _controller;

		/// <summary>
		/// Creates a new <see cref="budget.PaycheckInputDialog"/>
		/// It shows itself as soon as it's created.
		/// </summary>
		/// <param name="controller">The active controller</param>
		public PaycheckInputDialog (Controller controller) : base("Paycheck Input", null, DialogFlags.Modal)
		{
			_controller = controller;
			_label = new Label ("Enter how much money you got paid.");
			_entry = new Entry ("0.00");
			_button = new Button ("Enter");

			_entry.IsEditable = true;
			_entry.Visibility = true;
			_button.Clicked += new EventHandler (OnPaycheckEvent);

			this.VBox.Add (_label);
			this.VBox.Add (_entry);
			this.VBox.Add (_button);

			this.Resize (50, 150);
			this.ShowAll ();
		}

		/// <summary>
		/// Raised when _button is clicked.
		/// </summary>
		/// <param name="sender">The reference to the button that was pressed</param>
		/// <param name="events">A useless but required event</param>
		protected void OnPaycheckEvent(object sender, EventArgs events) 
		{
			double value = 0.00;
			try{
				value = Double.Parse(_entry.Text);
			}
			catch(Exception e)
			{
				DialogUtility.RunErrorDialog("Please enter a valid number.");
				Debug.Out (e.Message);
				return;
			}
			_controller.ProcessPaycheckInput(sender, value);
			this.Destroy ();
		}
	}
}

