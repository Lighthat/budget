﻿using System;
using Gtk;

namespace budget
{
	public partial class GetBudgetItemNameDialog : Dialog
	{

		Entry _entry;
		Label _label;
		Button _button;
		Controller _controller;

		string _itemName;

		public GetBudgetItemNameDialog (Controller controller) : base("Add Budget Item", null, DialogFlags.Modal)
		{
			_controller = controller;
			_label  = new Label  ("Enter a name for this budget item.");
			_entry  = new Entry  ("");
			_button = new Button ("Enter");

			_entry.IsEditable = true;
			_entry.Visibility = true;
			_button.Clicked += new EventHandler (OnNameEntryEvent);

			this.VBox.Add (_label);
			this.VBox.Add (_entry);
			this.VBox.Add (_button);

			this.Resize (50, 150);
			this.ShowAll ();
		}

		public string Value()
		{
			return _itemName;
		}

		protected void OnNameEntryEvent(object sender, EventArgs args)
		{
			Debug.Out("Name entered.");
			try{
				_itemName = _entry.Text;
				this.Destroy();
			}
			catch(Exception e)
			{
				DialogUtility.RunErrorDialog("Please enter a valid name.");
				Debug.Out (e.Message);
				return;
			}
		}
	}
}

