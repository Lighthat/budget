﻿using System;
using Gtk;
using System.Collections.Generic;
namespace budget
{
	/// <summary>
	/// A BudgetItem is a GTK Widget that makes it easy to add a new budget item
	/// to the UI.
	/// </summary>
	[System.ComponentModel.ToolboxItem (true)]
	public partial class BudgetItem : Gtk.Bin
	{
		Label _itemNameLabel;
		Label _allocatedMoneyValue;
		Label _amountPerCheckLabel;
		Label _amountPerCheckValue;
		Label _percentOfIncomeLabel;
		Label _percentOfIncomeValue;

		Button _addMoneyButton;
		Button _subtractMoneyButton;
		Button _setMoneyButton;
		Button _modifyAmountPerCheckButton;
		Button _modifyPercentOfIncomeButton;
		Button _deleteItemButton;

		Table _mainContainer;
		Table _infoContainer;
		Table _controlContainer;

		string _itemName;
		List<int> _hashCodeList = new List<int> ();

		public BudgetItem() : base()
		{

		}


		public BudgetItem (string itemName, Controller controller) : base ()
		{
			_itemName = itemName;
			_mainContainer    = new Table (1, 2, true);
			_infoContainer    = new Table (3, 2, true);
			_controlContainer = new Table (3, 3, true);

			_itemNameLabel        = new Label (_itemName + " - ");
			_allocatedMoneyValue  = new Label ("$0.00");
			_amountPerCheckLabel  = new Label ("Amount per check: ");
			_percentOfIncomeLabel = new Label ("Percent of income: ");
			_amountPerCheckValue  = new Label ("$0.00");
			_percentOfIncomeValue = new Label ("0%");

			_addMoneyButton              = new Button ("+");
			_subtractMoneyButton         = new Button ("-");
			_setMoneyButton              = new Button ("Set Money");
			_modifyAmountPerCheckButton  = new Button ("Modify");
			_modifyPercentOfIncomeButton = new Button ("Modify");
			_deleteItemButton            = new Button ("Delete");

			_hashCodeList.Add (_addMoneyButton.GetHashCode ());
			_hashCodeList.Add (_subtractMoneyButton.GetHashCode ());
			_hashCodeList.Add (_modifyAmountPerCheckButton.GetHashCode ());
			_hashCodeList.Add (_modifyPercentOfIncomeButton.GetHashCode ());
			_hashCodeList.Add (_setMoneyButton.GetHashCode ());
			_hashCodeList.Add (_deleteItemButton.GetHashCode ());

			_addMoneyButton.Clicked              += new EventHandler ( controller.AddMoney);
			_subtractMoneyButton.Clicked         += new EventHandler ( controller.SubtractMoney );
			_setMoneyButton.Clicked              += new EventHandler ( controller.SetMoney );
			_modifyAmountPerCheckButton.Clicked  += new EventHandler ( controller.ModifyAmountPerCheck );
			_modifyPercentOfIncomeButton.Clicked += new EventHandler ( controller.ModifyPercentOfIncome );
			_deleteItemButton.Clicked            += new EventHandler ( controller.DeleteItem );

			_mainContainer.Attach (_infoContainer,    0, 1, 0, 1);
			_mainContainer.Attach (_controlContainer, 1, 2, 0, 1);

			_infoContainer.Attach (_itemNameLabel,        0, 1, 0, 1);
			_infoContainer.Attach (_allocatedMoneyValue,  1, 2, 0, 1);
			_infoContainer.Attach (_amountPerCheckLabel,  0, 1, 1, 2);
			_infoContainer.Attach (_percentOfIncomeLabel, 0, 1, 2, 3);
			_infoContainer.Attach (_amountPerCheckValue,  1, 2, 1, 2);
			_infoContainer.Attach (_percentOfIncomeValue, 1, 2, 2, 3);

			_controlContainer.Attach (_addMoneyButton,              0, 1, 0, 1);
			_controlContainer.Attach (_subtractMoneyButton,         1, 2, 0, 1);
			_controlContainer.Attach (_setMoneyButton,              2, 3, 0, 1);
			_controlContainer.Attach (_modifyAmountPerCheckButton,  0, 1, 1, 2);
			_controlContainer.Attach (_modifyPercentOfIncomeButton, 0, 1, 2, 3);
			_controlContainer.Attach (_deleteItemButton,            2, 3, 2, 3);

			this.Add (_mainContainer);
		}

		public BudgetItem (string itemName, double amountPerCheck, int percentOfIncome, Controller controller) : base ()
		{
			_itemName = itemName;
			_mainContainer    = new Table (1, 2, true);
			_infoContainer    = new Table (3, 2, true);
			_controlContainer = new Table (3, 3, true);

			_itemNameLabel        = new Label (_itemName + " - ");
			_allocatedMoneyValue  = new Label ("$0.00");
			_amountPerCheckLabel  = new Label ("Amount per check: ");
			_percentOfIncomeLabel = new Label ("Percent of income: ");
			_amountPerCheckValue  = new Label (amountPerCheck.ToString("C"));
			_percentOfIncomeValue = new Label (percentOfIncome.ToString() + "%");

			_addMoneyButton              = new Button ("+");
			_subtractMoneyButton         = new Button ("-");
			_setMoneyButton              = new Button ("Set Money");
			_modifyAmountPerCheckButton  = new Button ("Modify");
			_modifyPercentOfIncomeButton = new Button ("Modify");
			_deleteItemButton            = new Button ("Delete");

			_hashCodeList.Add (_addMoneyButton.GetHashCode ());
			_hashCodeList.Add (_subtractMoneyButton.GetHashCode ());
			_hashCodeList.Add (_modifyAmountPerCheckButton.GetHashCode ());
			_hashCodeList.Add (_modifyPercentOfIncomeButton.GetHashCode ());
			_hashCodeList.Add (_setMoneyButton.GetHashCode ());
			_hashCodeList.Add (_deleteItemButton.GetHashCode ());

			_addMoneyButton.Clicked              += new EventHandler ( controller.AddMoney);
			_subtractMoneyButton.Clicked         += new EventHandler ( controller.SubtractMoney );
			_setMoneyButton.Clicked              += new EventHandler ( controller.SetMoney );
			_modifyAmountPerCheckButton.Clicked  += new EventHandler ( controller.ModifyAmountPerCheck );
			_modifyPercentOfIncomeButton.Clicked += new EventHandler ( controller.ModifyPercentOfIncome );
			_deleteItemButton.Clicked            += new EventHandler ( controller.DeleteItem );

			_mainContainer.Attach (_infoContainer,    0, 1, 0, 1);
			_mainContainer.Attach (_controlContainer, 1, 2, 0, 1);

			_infoContainer.Attach (_itemNameLabel,        0, 1, 0, 1);
			_infoContainer.Attach (_allocatedMoneyValue,  1, 2, 0, 1);
			_infoContainer.Attach (_amountPerCheckLabel,  0, 1, 1, 2);
			_infoContainer.Attach (_percentOfIncomeLabel, 0, 1, 2, 3);
			_infoContainer.Attach (_amountPerCheckValue,  1, 2, 1, 2);
			_infoContainer.Attach (_percentOfIncomeValue, 1, 2, 2, 3);

			_controlContainer.Attach (_addMoneyButton,              0, 1, 0, 1);
			_controlContainer.Attach (_subtractMoneyButton,         1, 2, 0, 1);
			_controlContainer.Attach (_setMoneyButton,              2, 3, 0, 1);
			_controlContainer.Attach (_modifyAmountPerCheckButton,  0, 1, 1, 2);
			_controlContainer.Attach (_modifyPercentOfIncomeButton, 0, 1, 2, 3);
			_controlContainer.Attach (_deleteItemButton,            2, 3, 2, 3);

			this.Add (_mainContainer);
		}

		public BudgetItem (string itemName, double allocatedMoney, double amountPerCheck, int percentOfIncome, Controller controller) : base ()
		{
			_itemName = itemName;
			_mainContainer    = new Table (1, 2, true);
			_infoContainer    = new Table (3, 2, true);
			_controlContainer = new Table (3, 3, true);

			_itemNameLabel        = new Label (_itemName + " - ");
			_allocatedMoneyValue  = new Label (allocatedMoney.ToString("C"));
			_amountPerCheckLabel  = new Label ("Amount per check: ");
			_percentOfIncomeLabel = new Label ("Percent of income: ");
			_amountPerCheckValue  = new Label (amountPerCheck.ToString("C"));
			_percentOfIncomeValue = new Label (percentOfIncome.ToString() + "%");

			_addMoneyButton              = new Button ("+");
			_subtractMoneyButton         = new Button ("-");
			_setMoneyButton              = new Button ("Set Money");
			_modifyAmountPerCheckButton  = new Button ("Modify");
			_modifyPercentOfIncomeButton = new Button ("Modify");
			_deleteItemButton            = new Button ("Delete");

			_hashCodeList.Add (_addMoneyButton.GetHashCode ());
			_hashCodeList.Add (_subtractMoneyButton.GetHashCode ());
			_hashCodeList.Add (_modifyAmountPerCheckButton.GetHashCode ());
			_hashCodeList.Add (_modifyPercentOfIncomeButton.GetHashCode ());
			_hashCodeList.Add (_setMoneyButton.GetHashCode ());
			_hashCodeList.Add (_deleteItemButton.GetHashCode ());

			_addMoneyButton.Clicked              += new EventHandler ( controller.AddMoney);
			_subtractMoneyButton.Clicked         += new EventHandler ( controller.SubtractMoney );
			_setMoneyButton.Clicked              += new EventHandler ( controller.SetMoney );
			_modifyAmountPerCheckButton.Clicked  += new EventHandler ( controller.ModifyAmountPerCheck );
			_modifyPercentOfIncomeButton.Clicked += new EventHandler ( controller.ModifyPercentOfIncome );
			_deleteItemButton.Clicked            += new EventHandler ( controller.DeleteItem );

			_mainContainer.Attach (_infoContainer,    0, 1, 0, 1);
			_mainContainer.Attach (_controlContainer, 1, 2, 0, 1);

			_infoContainer.Attach (_itemNameLabel,        0, 1, 0, 1);
			_infoContainer.Attach (_allocatedMoneyValue,  1, 2, 0, 1);
			_infoContainer.Attach (_amountPerCheckLabel,  0, 1, 1, 2);
			_infoContainer.Attach (_percentOfIncomeLabel, 0, 1, 2, 3);
			_infoContainer.Attach (_amountPerCheckValue,  1, 2, 1, 2);
			_infoContainer.Attach (_percentOfIncomeValue, 1, 2, 2, 3);

			_controlContainer.Attach (_addMoneyButton,              0, 1, 0, 1);
			_controlContainer.Attach (_subtractMoneyButton,         1, 2, 0, 1);
			_controlContainer.Attach (_setMoneyButton,              2, 3, 0, 1);
			_controlContainer.Attach (_modifyAmountPerCheckButton,  0, 1, 1, 2);
			_controlContainer.Attach (_modifyPercentOfIncomeButton, 0, 1, 2, 3);
			_controlContainer.Attach (_deleteItemButton,            2, 3, 2, 3);

			this.Add (_mainContainer);
		}

		protected override void OnSizeAllocated (Gdk.Rectangle allocation)
		{
			if (this.Child != null)
			{
				this.Child.Allocation = allocation;
			}
		}

		protected override void OnSizeRequested (ref Requisition requisition)
		{
			if (this.Child != null)
			{
				requisition = this.Child.SizeRequest ();
			}
		}

		public override string ToString()
		{
			return (_itemName + "," + AllocatedMoney() + "," + AmountPerCheck() + "," + PercentOfIncome());
		}

		public void AddMoney(double value)
		{
			double currentMoney = BudgetUtility.ParseMoneyFromLabel (_allocatedMoneyValue) + value;
			_allocatedMoneyValue.Text = currentMoney.ToString ("C");
		}

		public void SubtractMoney(double value)
		{
			double currentMoney = BudgetUtility.ParseMoneyFromLabel (_allocatedMoneyValue) - value;
			_allocatedMoneyValue.Text = currentMoney.ToString ("C");
		}

		public void AddFlatMoneyFromCheck()
		{
			double currentMoney = BudgetUtility.ParseMoneyFromLabel (_allocatedMoneyValue) + AmountPerCheck();
			_allocatedMoneyValue.Text = currentMoney.ToString ("C");
		}

		public void AddPercentMoneyFromCheck(double amount)
		{
			double currentMoney = BudgetUtility.ParseMoneyFromLabel (_allocatedMoneyValue);
			currentMoney += amount * (PercentOfIncome () / 100.0);
			_allocatedMoneyValue.Text = currentMoney.ToString ("C");
		}

		public string    ItemName()        {return _itemName;}

		public List<int> HashCodeList()    {return _hashCodeList;}

		public double    AllocatedMoney()  { return BudgetUtility.ParseMoneyFromLabel   (_allocatedMoneyValue); }

		public double    AmountPerCheck()  { return BudgetUtility.ParseMoneyFromLabel   (_amountPerCheckValue); }

		public int       PercentOfIncome() { return BudgetUtility.ParsePercentFromLabel (_percentOfIncomeValue); }

		public void      SetAllocatedMoney (double value) { _allocatedMoneyValue.Text = value.ToString("C"); }

		public void      SetAmountPerCheck (double value) { _amountPerCheckValue.Text = value.ToString("C"); }

		public void      SetPercentOfIncome(int value)    { _percentOfIncomeValue.Text = value.ToString() + "%"; }

	}
}

