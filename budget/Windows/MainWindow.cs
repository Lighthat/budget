﻿using System;
using Gtk;
using System.Collections.Generic;
namespace budget
{
	public partial class MainWindow : Gtk.Window
	{
		Label  _label;
		Button _button;
		Button _addBudgetItemButton;
		Button _inputPaycheckButton;
		Button _saveButton;
		Button _dataFileButton;
		Table  _mainTable;
		Table  _itemTable;
		Table  _footerTable;
		ScrolledWindow _itemContainer;



		public MainWindow (Controller controller) :
			base (Gtk.WindowType.Toplevel)
		{
			Debug.Out ("This is some test text on MainWindow construction.");
			this.Resize (800, 600);
			this.DeleteEvent += OnDelete;
			_itemContainer = new ScrolledWindow ();
			_itemContainer.SetPolicy (PolicyType.Never, PolicyType.Always);
			_mainTable     = new Table (20, 1, false); 
			_itemTable     = new Table (0, 1, true);
			_footerTable   = new Table (1, 6, false);

			_itemTable.RowSpacing = 20;
			_itemContainer.AddWithViewport (_itemTable);

			_label  = new Label  ("0");
			_button = new Button (_label);
			_addBudgetItemButton = new Button ("Add Item");
			_inputPaycheckButton = new Button ("Input Check");
			_saveButton          = new Button ("Save");
			_dataFileButton      = new Button ("Data File");

			_saveButton.Clicked += new EventHandler ( controller.Save );
			_inputPaycheckButton.Clicked += new EventHandler (controller.InputPaycheck);
			_addBudgetItemButton.Clicked += new EventHandler (controller.AddBudgetItem);

			_footerTable.Attach (_button, 4, 5, 0, 1);
			_footerTable.Attach (_addBudgetItemButton, 0, 1, 0, 1);
			_footerTable.Attach (_inputPaycheckButton, 1, 2, 0, 1);
			_footerTable.Attach (_saveButton, 2, 3, 0, 1);
			_footerTable.Attach (_dataFileButton, 3, 4, 0, 1);

			_mainTable.Attach (_itemContainer,    0, 1, 0, 18);
			_mainTable.Attach (_footerTable,  0, 1, 19, 20);

			this.Add (_mainTable);

			this.ShowAll ();
		}

		/// <summary>
		/// Adds a budget item to the display table
		/// </summary>
		/// <param name="item">The item you wish to add</param>
		public void AddBudgetItem(BudgetItem item)
		{
			_itemTable.Attach (item, 0, 1, _itemTable.NRows - 1, _itemTable.NRows);
			_itemTable.Resize (_itemTable.NRows + 1, 1);
			this.ShowAll ();

		}

		/// <summary>
		/// Removes a budget item from the display table
		/// </summary>
		/// <param name="item">The item you wish to remove</param>
		public void RemoveBudgetItem(BudgetItem item)
		{
			Widget[] itemList = _itemTable.Children;
			foreach(BudgetItem __item in itemList)
			{
				if (__item.ItemName () == item.ItemName ())
					_itemTable.Remove (__item);
			}
		}

		/// <summary>
		/// Resizes the table so that it can fit the number of BudgetItems it contains.
		/// Currently this method is not used.
		/// </summary>
		public void ResizeItemTable()
		{
			Widget[] itemList = _itemTable.Children;
			foreach(BudgetItem item in itemList)
			{
				_itemTable.Remove (item);
			}
			_itemTable.Resize (1, 1);
			foreach (BudgetItem item in itemList)
			{
				Debug.Out (_itemTable.NRows + "");
				AddBudgetItem (item);
			}
			this.ShowAll ();
		}

			
		/// <summary>
		/// Describes the behavior of the application when the window is closed.
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <param name="arguments">Arguments.</param>
		static void OnDelete(object obj, DeleteEventArgs arguments)
		{
			Application.Quit ();
		}
			
	}



}

