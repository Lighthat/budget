﻿using System;
using Gtk;
using System.Collections.Generic;

namespace budget
{
	public class Controller
	{
		Model _model;
		View  _view;

		public Controller ()
		{
			_model = new Model ();
			_view = new View (this, _model);
			_model.Load (this, _view);
		}

		public void AddBudgetItem(object obj, EventArgs arguments)
		{
			Debug.Out ("Add Item Clicked");
			_model.AddBudgetItem (DialogUtility.RunAddItemDialog (this));
		}

		public void AddMoney(object obj, EventArgs arguments)
		{
			Debug.Out ("Add money clicked");
			Debug.Out (BudgetUtility.FindButtonOwner (obj.GetHashCode (), _model.ItemList ()).ItemName());

		}

		public void SubtractMoney(object obj, EventArgs arguments)
		{
			Debug.Out ("Subtract money clicked");
		}

		public void SetMoney(object obj, EventArgs arguments)
		{
			Debug.Out ("Set money clicked");
		}

		public void ModifyAmountPerCheck(object obj, EventArgs arguments)
		{
			Debug.Out ("Modify money clicked");
		}

		public void ModifyPercentOfIncome(object obj, EventArgs arguments)
		{
			Debug.Out ("Modify percent clicked");
		}

		public void DeleteItem(object obj, EventArgs arguments)
		{
			Debug.Out ("Delete clicked");
			try{
				_model.RemoveBudgetItem (BudgetUtility.FindButtonOwner (obj.GetHashCode (), _model.ItemList ()));
			}
			catch(Exception e) {
				Debug.Out (e.Message);
			}
		}

		public void Save(object obj, EventArgs arguments)
		{
			BudgetUtility.Save (_model.ItemList(), _model.DataFilePath());
			DialogUtility.RunSaveDialog ();
		}

		public void InputPaycheck(object obj, EventArgs arguments)
		{
			DialogUtility.RunPaycheckInputDialog (this);
		}

		public void ProcessPaycheckInput(object obj, double amount)
		{
			Debug.Out ("Paycheck dialog entered. " + amount);
			_model.AllocatePaycheck (amount);
		}
	}
}

