﻿using System;
using System.Collections.Generic;

namespace budget
{
	public class View : IObserver<string>
	{
		private Controller _controller;
		private IDisposable _unsubscriber;
		private MainWindow _mainWindow;

		public View (Controller controller, Model model)
		{
			_controller = controller;
			_mainWindow = new MainWindow (_controller);
			Subscribe (model);
		}

		public void AddBudgetItem(BudgetItem item)
		{
			_mainWindow.AddBudgetItem (item);
		}

		public void RemoveBudgetItem(BudgetItem item)
		{
			_mainWindow.RemoveBudgetItem (item);
		}

		public MainWindow GetWindow()
		{
			return _mainWindow;
		}


		/// <summary>
		/// Allows the View to observe the Model
		/// </summary>
		/// <param name="provider">The Model</param>
		public void Subscribe(IObservable<string> provider)
		{
			Debug.Out ("Subscribe in view.");

			if (provider != null) 
				_unsubscriber = provider.Subscribe(this);
		}

		/// <summary>
		/// Raises the completed event.
		/// </summary>
		public void OnCompleted()
		{
			Debug.Out ("On Completed.");
			this.Unsubscribe();
		}

		/// <summary>
		/// Raises the error event.
		/// </summary>
		/// <param name="e">E.</param>
		public void OnError(Exception e)
		{
			Debug.Out (e.Message);
		}

		/// <summary>
		/// This handles an event from the Model. It will tell the Window what to do next.
		/// </summary>
		/// <param name="window">The command that was issued</param>
		public void OnNext(string command)
		{
			if (command == "save") {

			} else if (command.Substring (0, 4) == "init") {
				
			} else if (command.Substring (0, 8) == "add_item") {
				Debug.Out (command);
				AddBudgetItem (BudgetUtility.ParseBudgetItemFromString (command.Substring (10), _controller));
			} else if (command.Substring (0, 11) == "remove_item") {
				Debug.Out (command);
				RemoveBudgetItem (BudgetUtility.ParseBudgetItemFromString (command.Substring (13), _controller));
			}
			Debug.Out ("issued command: " + command);
		}

		/// <summary>
		/// Cleans up the Model on program close.
		/// </summary>
		public void Unsubscribe()
		{
			_unsubscriber.Dispose();
		}


	}
}

