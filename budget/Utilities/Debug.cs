﻿using System;

namespace budget
{
	/// <summary>
	/// This class supplies a debug utility that will allow you to set messages to be printed
	/// to the system console while also letting you both enable and disable all of these messages
	/// to make debugging production code easier.
	/// </summary>
	public static class Debug
	{
		private static bool   _enabled = false;
		private static String _debugPrefix = "** DEBUG: ";

		/// <summary>
		/// Prints the debug text if debug text is enabled.
		/// </summary>
		/// <param name="text">The text you wish to print</param>
		public static void Out(String text)
		{
			if (_enabled) {
				Console.ForegroundColor = ConsoleColor.DarkMagenta;
				Console.WriteLine (_debugPrefix + text);
				Console.ResetColor();
			}
		}

		/// <summary>
		/// Enables debug text
		/// </summary>
		public static void Enable()  { _enabled = true;  }

		/// <summary>
		/// Disables debug text
		/// </summary>
		public static void Disable() { _enabled = false; }

		/// <summary>
		/// Sets the prefix for every debug message printed to the console.
		/// </summary>
		/// <param name="text">The desired prefix</param>
		public static void Prefix(String text) { _debugPrefix = text; }

		/// <summary>
		/// Sets the prefix for every debug message printed to the console to the
		/// default of "** DEBUG: "
		/// </summary>
		public static void Prefix() { _debugPrefix = "** DEBUG: "; }
	}
}

