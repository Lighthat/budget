﻿using System;
using System.Collections.Generic;
using Gtk;
namespace budget
{
	/// <summary>
	/// This utility will provide multiple functions that might be helpful for miscellaneous tasks,
	/// like interfacing with custom widgets. It's also a good place to put methods that don't quite fit
	/// well in any other class.
	/// </summary>
	public static class BudgetUtility
	{
		/// <summary>
		/// Used to identify which BudgetItem sent a signal by checking through the
		/// hash codes of all of the buttons in a given BudgetItem.
		/// </summary>
		/// <returns>The button owner.</returns>
		/// <param name="buttonHashCode">The hash code you want to check.</param>
		/// <param name="itemList">The list of BudgetItems in the program.</param>
		public static BudgetItem FindButtonOwner(int buttonHashCode, List<BudgetItem> itemList)
		{
			foreach (BudgetItem item in itemList)
			{
				foreach (int i in item.HashCodeList()) 
				{
					if (i == buttonHashCode)
						return item;
				}
			}
			return null;
		}

		public static void Save(List<BudgetItem> itemList, string dataFile)
		{
			string itemStream = "";
			foreach (BudgetItem item in itemList) 
			{
				itemStream += item.ItemName() +	"," + item.AllocatedMoney() + "," + item.AmountPerCheck() +	"," + item.PercentOfIncome() + Environment.NewLine;
			}
			try
			{
				if(!System.IO.File.Exists(@dataFile))
				{
					var file = System.IO.File.Create (@dataFile);
					file.Close();
				}
				System.IO.File.WriteAllText(@dataFile, itemStream);
			}
			catch(Exception e)
			{
				Debug.Out (e.ToString ());
			}
		}

		public static List<BudgetItem> Load(string dataFile, Controller controller)
		{
			List<BudgetItem> itemList = new List<BudgetItem> ();
			string[] items = System.IO.File.ReadAllLines (@dataFile);
			foreach (string s in items) 
			{
				string[] itemProperties = s.Split (',');
				BudgetItem item = new BudgetItem (itemProperties[0], controller);
				item.SetAllocatedMoney (Double.Parse(itemProperties [1]));
				item.SetAmountPerCheck (Double.Parse(itemProperties [2]));
				item.SetPercentOfIncome(Int32.Parse (itemProperties [3]));
				itemList.Add (item);
			}
			return itemList;
		}

		public static BudgetItem ParseBudgetItemFromString(string _string, Controller controller)
		{
			string[] attributes = _string.Split (',');
			try{
				BudgetItem item = new BudgetItem (attributes [0], Double.Parse(attributes [1]), Double.Parse(attributes [2]), Int32.Parse(attributes [3]), controller);
				return item;
			}
			catch(Exception e) {
				DialogUtility.RunErrorDialog ("An error occurred when trying to create a Budget Item from a string.");
				Debug.Out (e.Message);
				return null;
			}
		}

		public static double ParseMoneyFromLabel(Label label)
		{
			string text = label.Text;
			try
			{
				return Double.Parse(text, System.Globalization.NumberStyles.Currency);
			}
			catch(Exception e) 
			{
				Debug.Out (e.ToString());
			}
			return -123456789.0;
		}

		public static int ParsePercentFromLabel(Label label)
		{
			try
			{
				return Int32.Parse (label.Text.Trim ('%'));
			}
			catch(Exception e)
			{
				Debug.Out (e.ToString ());
			}
			return -1;
		}

		public static string CreateInitString(List<BudgetItem> itemList)
		{
			string itemStream = "";
			foreach (BudgetItem item in itemList)
			{
				itemStream += item.ItemName() +	"," + item.AllocatedMoney() + "," + item.AmountPerCheck() +	"," + item.PercentOfIncome() + ";";
			}
			return itemStream;
		}
	}
}

