﻿using System;
using Gtk;


namespace budget
{
	/// <summary>
	/// This class will serve as a utility for showing dialogs and collecting information from the user.
	/// </summary>
	public static class DialogUtility
	{

		public static BudgetItem RunAddItemDialog(Controller controller)
		{
			GetBudgetItemNameDialog  nameDialog = new GetBudgetItemNameDialog (controller);
			nameDialog.Run ();
			GetAmountPerCheckDialog  amountPerCheckDialog = new GetAmountPerCheckDialog (controller);
			amountPerCheckDialog.Run ();
			GetPercentOfIncomeDialog percentOfIncomeDialog = new GetPercentOfIncomeDialog (controller);
			percentOfIncomeDialog.Run ();
			return new BudgetItem (nameDialog.Value (), amountPerCheckDialog.Value (), percentOfIncomeDialog.Value (), controller);
		}

		public static void RunAddMoneyDialog(BudgetItem item)
		{

		}

		public static void RunPaycheckInputDialog(Controller controller)
		{
			PaycheckInputDialog dialog = new PaycheckInputDialog (controller);
			dialog.Run ();
			dialog.Destroy ();
		}

		public static void RunSaveDialog()
		{
			MessageDialog saveInfoDialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Data successfully saved.");
			saveInfoDialog.Run ();
			saveInfoDialog.Destroy();
						
		}

		public static bool RunDeleteConfirmDialog(BudgetItem item)
		{
			return false;
		}

		public static void RunErrorDialog(string errorMessage)
		{
			MessageDialog paycheckErrorDialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, errorMessage);
			paycheckErrorDialog.Run ();
			paycheckErrorDialog.Destroy (); 
		}
	}
}

