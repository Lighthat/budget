﻿using System;
using Gtk;
namespace budget
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			Debug.Enable ();
			Controller controller = new Controller ();
			Application.Run ();
		}
	}
}